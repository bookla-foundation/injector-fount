from os.path import abspath, dirname
from setuptools import setup
from injectorfount import __version__

root = dirname(abspath(__file__))


def execfile(fname, globs, locs=None):
    locs = locs or globs
    exec(compile(open(fname).read(), fname, "exec"), globs, locs)


version = __version__
required = '''
injector==0.19.0
typing-extensions==4.2.0 ; python_version < '3.9'
'''


setup(
    name="injectorfount",
    version=version,
    python_requires='>=3.6',
    description='',
    classifiers=[
        'Development Status :: Stable',
        'License :: OSI Approved :: MIT',
        'Programming Language :: Python :: 3.7',
        'Intended Audience :: Developers'
    ],
    packages=['injectorfount'],
    include_package_data=False,
    package_data={
        'injectorfount': ['./*.txt']
    },
    keywords=[
        'Injector',
        'Injector Provider'
        'Dependency Injection',
        'DI',
        'Dependency Injection framework',
        'Inversion of Control',
        'IoC',
        'Inversion of Control container',
    ],
    install_requires=required,
    license='MIT'
)
